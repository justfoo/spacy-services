#!/usr/bin/python3

# Import required libraries
import pandas   as pd
import numpy    as np
import spacy
from   datetime import datetime
from   spacy    import displacy

nlp = spacy.load('en_core_web_lg')

##########################################################################################################
# Do Stuff 
##########################################################################################################
startTime = datetime.now()


#Get Doc Corpus
dfCorpus = pd.read_excel('corpus.xlsx')

#Clean up the dataframe a bit. Get rid of events in the past and replace NaNs with ''
dfCorpus = dfCorpus.replace(np.nan, '', regex=True)
#dfCorpus = dfCorpus[ (dfCorpus['Category'] != 'Event') | (dfCorpus['Date'] > datetime.now()) ]

#Construct a column with all text content
dfCorpus['allContent'] = dfCorpus['Title'] \
    +'\r\n'+ dfCorpus['SubTitle']  \
    +'\r\n'+ dfCorpus['Author']  \
    +'\r\n'+ dfCorpus['Content'] \
    +'\r\n'+ dfCorpus['SideMatter'] \
    +'\r\n'+ dfCorpus['Tags']

loadTime = datetime.now()

#Analyze the combined text with spacy
dfCorpus['NLP'] = dfCorpus.allContent.apply(nlp)

NLPTime = datetime.now()

#Create lists for similarity object
simURL = []
simTitle = []
simCategory = []
simDate = []
simVector = []
simVectorNorm = []

#populate the lists
for i in dfCorpus.index:
    simURL.append( dfCorpus['URL'][i] )
    simTitle.append( dfCorpus['Title'][i] )
    simCategory.append( dfCorpus['Category'][i] )
    simDate.append( dfCorpus['Date'][i] )
    simVector.append( dfCorpus['NLP'][i].vector )
    simVectorNorm.append( dfCorpus['NLP'][i].vector_norm )

#Costruct a dataframe with the list values
#preserving only the vectors allows us to do similiarity comparisons without having to load the ginormous
#NLP object with all the processed model information.
dfSimilarity = pd.DataFrame()
dfSimilarity['URL'] = simURL
dfSimilarity['Title'] = simTitle
dfSimilarity['Category'] = simCategory
dfSimilarity['Date'] = simDate
dfSimilarity['Vector'] = simVector
dfSimilarity['VectorNorm'] = simVectorNorm

#Save the vectors to disk
dfSimilarity.to_pickle("./dfSimilarity.pkl")

endTime = datetime.now()

##########################################################################################################
# Print log of time-hacks
##########################################################################################################

runDelta_load = loadTime - startTime
runDelta_NLP = NLPTime - loadTime
runDelta_save = endTime - NLPTime
runDelta_total = endTime - startTime

print(str(len(dfSimilarity)) + " records loaded")
print("-----")
print( "{:3.2f} seconds to load"   .format( runDelta_load.total_seconds()  ) )
print( "{:3.2f} seconds to process".format( runDelta_NLP.total_seconds()   ) )
print( "{:3.2f} seconds to save"   .format( runDelta_save.total_seconds()  ) )
print( "{:3.2f} seconds TOTAL"     .format( runDelta_total.total_seconds() ) )

#!/usr/bin/python3

# Import required libraries
import datefinder
import requests
import cssutils
import time
import pandas   as     pd
import numpy    as     np
from   bs4      import BeautifulSoup
from   datetime import datetime


##########################################################################################################
# Housekeeping - prep some basic components to be used throughout
##########################################################################################################
# --optional, to let web admin reach out if they want to
headers = {
    'User-Agent': 'Scraping by Justin Williams, taliesin@uw.edu',
    'From': 'taliesin@uw.edu'
}

#Which properties to scrape?
doColumns = True
doUWToday = True
doKUOW = True
doEvents = True

#Number of seconds to wait between HTTP requests
wait_interval = 2


#Prep the lists that will become elements in the dataframe
df_ArticleURL = []
df_ArticleTitle = []
df_ArticleSubTitle = []
df_ArticleAuthor = []
df_ArticleDate = []
df_ArticleFirstImage = []
df_ArticleTags = []
df_ArticleSideMatter = []
df_ArticleContent = []
df_ArticleCategory = []

##########################################################################################################
# UW Columns
##########################################################################################################
if doColumns is True:
    startTime = datetime.now()

    ### Create a list of 'index' pages which contain the links to the content we want to see
    pages_list = [
                  "https://magazine.washington.edu/category/people/"
                 ,"https://magazine.washington.edu/category/features/"
                 ,"https://magazine.washington.edu/category/hub/"
                 ,"https://magazine.washington.edu/category/solutions/"
                 ,"https://magazine.washington.edu/category/ae/"
                 ,"https://magazine.washington.edu/category/sports/"
                 ,"https://magazine.washington.edu/category/letters/"
                 ]

    scrapeSource = "UW Columns"
    scrapeCategory = "Magazine Article"

    all_links_list = []
    for target_url in pages_list:
        page = requests.get(target_url)
        soup = BeautifulSoup(page.content, 'lxml')
        for a in soup.find_all('a', href=True):
            if "magazine.washington.edu/feature"  in a['href']: 
                all_links_list.append(a['href'])
        time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server


    #Set() will remove duplicates from a list, list() will convert the set back to a list
    unique_links_list = list(set(all_links_list))


    #Now that we have all links from the index pages - load each one up in sequence and extract the content from it.
    for articleURL in unique_links_list[0:]: ##### NOTE if needed you can filter to desired number of results

        # get page text and parse w/ BeautifulSoup
        page = requests.get(articleURL)
        soup = BeautifulSoup(page.text, 'lxml') #lxml parser seems to work better then native html parser

        # Remove known chaff - trying to minimize risk of including non-content in the results
        for x in soup.find_all("head"): x.decompose()
        for x in soup.find_all("script"): x.decompose()
        for x in soup.find_all("noscript"): x.decompose()
        for x in soup.find_all("div",{"class":"container-fluid"}): x.decompose()
        for x in soup.find_all("div",{"class":"social-share-container"}): x.decompose()
        for x in soup.find_all("div",{"id":"fb-reset"}): x.decompose()
        for x in soup.find_all("div",{"id":"disqus_thread"}): x.decompose()
        for x in soup.find_all("div",{"class":"uw-footer"}): x.decompose()
 
        #Append URL to array
        df_ArticleURL.append(articleURL)


        #Append Category Info - most will be the same for every item from the same source
        df_ArticleCategory.append(scrapeCategory)
    
    
        #Append title, if it exists
        soupTitle = soup.find("h1",{"class":"feature-title"})
        if soupTitle:
            df_ArticleTitle.append(soupTitle.get_text().strip())
            soupTitle.decompose()
        else: df_ArticleTitle.append("")


        #Append sub title, if it exists
        soupSubTitle = soup.find("p",{"class":"teaser-intro"})
        if soupSubTitle:
            df_ArticleSubTitle.append(soupSubTitle.get_text().strip())
            soupSubTitle.decompose()
        else: df_ArticleSubTitle.append("")


        #Get Byline, extract parts from it
        soupByline = soup.find("p",{"class":"byline"})
        if soupByline:
        #    bylineParts = soupByline.get_text().split("|")
        #    try:
        #        df_ArticleAuthor.append(bylineParts[0].strip())
        #    except IndexError:
        #        df_ArticleAuthor.append("")
        #    try:
        #        df_ArticleDate.append(datetime.strptime(bylineParts[1].strip(), '%B %d, %Y' ))
        #    except IndexError:
        #        df_ArticleDate.append("")
            df_ArticleAuthor.append(soupByline.get_text().strip())
            soupByline.decompose()
        else:
            df_ArticleAuthor.append("")
        df_ArticleDate.append("")
    
    
        #AppendTag Info, if it exists
        df_ArticleTags.append("")


        # get Headline Image URL
        imgContainer = soup.find('div', {'class': 'headline-image'}) 
        if imgContainer :
            cssContainer = cssutils.parseStyle(imgContainer['style'])
            imgUrl = cssContainer.getPropertyValue('background-image').replace('url(', '').replace(')', '')
            df_ArticleFirstImage.append(imgUrl)
            imgContainer.decompose()
        else:
            df_ArticleFirstImage.append("")
 

        #AppendSideMatter, if it exists
        sideMatterStr=""

        soupCaption = soup.find_all("p",{"class":"wp-caption-text"})
        if soupCaption: 
            for tag in soupCaption:
                sideMatterStr = sideMatterStr + " | " + tag.get_text().strip()
                tag.decompose()

        soupCaption = soup.find_all("p",{"class":"final-byline"})
        if soupCaption: 
            for tag in soupCaption:
                sideMatterStr = sideMatterStr + " | " + tag.get_text().strip()
                tag.decompose()

        if len(sideMatterStr)>0: df_ArticleSideMatter.append(sideMatterStr[3:])
        else: df_ArticleSideMatter.append("")


        #Everything we haven't decomposed inside the main content wrapper is the 'content'
        soupContent = soup.find("div",{"class":"columns-feature"})
        if soupContent:
            df_ArticleContent.append(soupContent.get_text().strip())
        else: df_ArticleContent.append("")
        
        time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server


    endTime = datetime.now()
    runTime = endTime - startTime
    print("----------------------------------------------------------------------------------------------------------")
    print( "{:2.2f} minutes to scrape "   .format( runTime.total_seconds()  / 60 ) + "{:,} URLs from ".format(len(unique_links_list)) + scrapeSource)



##########################################################################################################
# UW Today
##########################################################################################################
if doUWToday is True:
    startTime = datetime.now()


    # Generates a list of links to pages that are themselves an index of links ot the actual content
    # handling paginated list
    # https://stackoverflow.com/questions/45013275/scraping-paginated-pages-using-python-beautiful-soup?rq=1

    scrapeSource = "UW Today"
    scrapeCategory = "News Article" # everything scraped from UW Today will called a News Article.

    all_links_list = []

    page_num = 1
    max_page_num = 25
    wait_interval = 5
    http_status_okay = True

    while http_status_okay:
    
        url = "https://www.washington.edu/news/the-latest-news-from-the-uw/page/"+str(page_num)+"/"
        page=requests.get(url)
    
        soup = BeautifulSoup(page.content, 'lxml')
        all_a = soup.find_all('a', {'class': 'more'})
    
        for a in all_a:
            if  "Read more" in a.get_text():
                all_links_list.append(a['href'])

        page_num = page_num + 1
    
        time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server
    
        # continue if we get a 200 response code
        if page.status_code is 200 and page_num <= max_page_num : 
            http_status_okay = True
        else:
            http_status_okay = False
        
    #Set() will remove duplicates from a list, list() will convert the set back to a list
    unique_links_list = list(set(all_links_list))
    #for link in unique_links_list: print(link)

    for articleURL in unique_links_list[0:]: ##### NOTE if needed you can filter to desired number of results

        # get page text and parse w/ BeautifulSoup
        page = requests.get(articleURL)
        soup = BeautifulSoup(page.text, 'lxml') #lxml parser seems to work better then native html parser

        # Remove known chaff - trying to minimize risk of including non-content in the results
        for x in soup.find_all("head"): x.decompose()
        for x in soup.find_all("script"): x.decompose()
        for x in soup.find_all("nav"): x.decompose()
        for x in soup.find_all("header"): x.decompose()
        for x in soup.find_all("small"): x.decompose()
        for x in soup.find_all("div",{"id":"uwsearcharea"}): x.decompose()
        for x in soup.find_all("div",{"id":"disqus_thread"}): x.decompose()
        for x in soup.find_all("div",{"id":"sharing_email"}): x.decompose()
        for x in soup.find_all("div",{"class":"uw-news-image"}): x.decompose()
        for x in soup.find_all("div",{"class":"uw-site-title"}): x.decompose()
        for x in soup.find_all("div",{"class":"sharing"}): x.decompose()
        for x in soup.find_all("div",{"class":"uw-footer"}): x.decompose()
        for x in soup.find_all("div",{"class":"uw-sidebar"}): x.decompose()

        
        #Append URL to array
        df_ArticleURL.append(articleURL)
    
    
        #Append Category Info - most will be the same for every item from the same source
        df_ArticleCategory.append(scrapeCategory)

    
        #Append title, if it exists
        soupTitle = soup.find("h1",{"class":"entry-title"})
        if soupTitle:
            df_ArticleTitle.append(soupTitle.get_text().strip())
            soupTitle.decompose()
        else: df_ArticleTitle.append("")

    
        #Append SubTitle, if it exists
        df_ArticleSubTitle.append("")
    
    
        #Append Author Info, if it exists
        soupAuthor = soup.find("div",{"class":"author-info"})
        if soupAuthor:
            df_ArticleAuthor.append(soupAuthor.get_text().strip())
            soupAuthor.decompose()
        else: df_ArticleAuthor.append("")

    
        #Append Date Info, if it exists
        soupDate = soup.find("p",{"class":"date"})
        if soupDate:
            df_ArticleDate.append( datetime.strptime(  soupDate.get_text().strip() , '%B %d, %Y' )   )
            soupDate.decompose()
        else: df_ArticleDate.append("")
    
    
        #AppendTag Info, if it exists
        soupTags = soup.find_all("a",{"rel":"tag"})
        tagStr=""
        for tag in soupTags:
            tagStr = tagStr + " | " + tag.get_text().strip()
            tag.decompose()
        if len(tagStr)>0: df_ArticleTags.append(tagStr[3:])
        else: df_ArticleTags.append("")
        for x in soup.find_all("p",{"class":"story-tags"}): x.decompose()        
    

        #Append Image link
        soupImg = soup.find("img",src=True)
        if soupImg : df_ArticleFirstImage.append(soupImg["src"])
        else: df_ArticleFirstImage.append("")
    

        #AppendSideMatter, if it exists
        sideMatterStr=""

        soupCaption = soup.find_all("div",{"class":"wp-caption"})
        if soupCaption: 
            for tag in soupCaption:
                sideMatterStr = sideMatterStr + " | " + tag.get_text().strip()
                tag.decompose()

        soupVideo = soup.find_all("div",{"class":"nc-video-player"})
        if soupVideo: 
            for tag in soupVideo:
                sideMatterStr = sideMatterStr + " | " + tag.get_text().strip()
                tag.decompose()

        soupAside = soup.find_all("aside")
        if soupAside: 
            for tag in soupAside:
                sideMatterStr = sideMatterStr + " | " + tag.get_text().strip()
                tag.decompose()

        if len(sideMatterStr)>0: df_ArticleSideMatter.append(sideMatterStr[3:])
        else: df_ArticleSideMatter.append("")
       
    
        #Everything we haven't decomposed inside the main content wrapper is the 'content'
        soupContent = soup.find("div",{"id":"main_content"})
        if soupContent:
            df_ArticleContent.append(soupContent.get_text().strip())
        else: df_ArticleContent.append("")
            
        time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server


    endTime = datetime.now()
    runTime = endTime - startTime
    print("----------------------------------------------------------------------------------------------------------")
    print( "{:2.2f} minutes to scrape "   .format( runTime.total_seconds()  / 60 ) + "{:,} URLs from ".format(len(unique_links_list)) + scrapeSource)



##########################################################################################################
# KUOW
##########################################################################################################
if doKUOW is True:
    startTime = datetime.now()

    # Generates a list of links to pages that are themselves an index of links ot the actual content
    # handling paginated list
    # https://stackoverflow.com/questions/45013275/scraping-paginated-pages-using-python-beautiful-soup?rq=1

    scrapeSource = "KUOW"
    scrapeCategory = "KUOW Content" # This totally glosses overy any content categorization...
                                    # maybe future improvment is to understand podcast vs news, etc.

    all_links_list = []

    max_page_num = 20

    #Each of these is a base for a paginated list of other pages to go through
    # hence walking the array, and then inside each step, iterating through the sub pages
    indexPageList = [
         'https://www.kuow.org/series/burning-question?page='
        ,'https://www.kuow.org/series/mother-road?page='
        ,'https://www.kuow.org/series/SSP?page='
        ,'https://www.kuow.org/series/ask?page='
        ,'https://www.kuow.org/series/region-of-boom?page='
        ,'https://www.kuow.org/series/soundqs?page='
        ,'https://www.kuow.org/series/curiosityclub?page='
    ]

    hostnameBaseURL = 'https://www.kuow.org'

    for baseLink in indexPageList:
        page_num = 1
        http_status_okay = True
        while http_status_okay:
            url = baseLink + str(page_num)
            page = requests.get(url)
            soup = BeautifulSoup(page.content, 'lxml')
            
            for h3a in soup.findAll('a'):
                if h3a.parent.name == 'h3':
                    all_links_list.append(hostnameBaseURL + h3a["href"])
            
            page_num=page_num+1
            time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server
    
            # continue if we get a 200 response code
            if page.status_code is 200 and page_num <= max_page_num : 
                http_status_okay = True
            else:
                http_status_okay = False


            
    #Set() will remove duplicates from a list, list() will convert the set back to a list
    unique_links_list = list(set(all_links_list))

    for articleURL in unique_links_list[0:]: ##### NOTE if needed you can filter to desired number of results

        # get page text and parse w/ BeautifulSoup
        page = requests.get(articleURL)
        soup = BeautifulSoup(page.text, 'lxml') #lxml parser seems to work better then native html parser

        
        #Append URL to array
        df_ArticleURL.append(articleURL)
    
    
        #Append Category Info - most will be the same for every item from the same source
        df_ArticleCategory.append(scrapeCategory)

    
        #Append title, if it exists
        soupTitle = soup.find("h1",{"class":"headline"})
        if soupTitle:
            df_ArticleTitle.append(soupTitle.get_text().strip())
            soupTitle.decompose()
        else: df_ArticleTitle.append("")

    
        #Append SubTitle, if it exists
        df_ArticleSubTitle.append("")
    
    
        #Append Author Info, if it exists
        soupAuthor = soup.find("ul",{"class":"authors"})
        if soupAuthor:
            df_ArticleAuthor.append(soupAuthor.get_text().strip())
            soupAuthor.decompose()
        else: df_ArticleAuthor.append("")

    
        #Append Date Info, if it exists
        soupDate = soup.find("time",datetime=True)
        if soupDate:
            df_ArticleDate.append(soupDate["datetime"])
            soupDate.decompose()
        else: df_ArticleDate.append("")
    
        #Pull the first image out of the slideshow
        soupIMG = soup.find("img",srcset=True)
        if soupIMG:
            df_ArticleFirstImage.append(soupIMG["srcset"].split("?")[0])
            soupDate.decompose()
        else: df_ArticleFirstImage.append("")

        
        df_ArticleSideMatter.append("")
        
    
        soupTags = soup.find("script",{"data-key":"tags"})
        if soupTags:
            df_ArticleTags.append(soupTags["data-value"].strip().replace("|"," | "))
            soupTags.decompose()
        else:
            df_ArticleTags.append("")
        
        
        #Everything we haven't decomposed inside the main content wrapper is the 'content'
        soupContent = soup.find("div",{"class":"story-content-wrapper"})
        for x in soupContent.find_all("section",{"class":"ad_placement"}): x.decompose()
        if soupContent:
            df_ArticleContent.append(soupContent.get_text().strip())
        else: df_ArticleContent.append("")
        
        time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server

    endTime = datetime.now()
    runTime = endTime - startTime
    print("----------------------------------------------------------------------------------------------------------")
    print( "{:2.2f} minutes to scrape "   .format( runTime.total_seconds()  / 60 ) + "{:,} URLs from ".format(len(unique_links_list)) + scrapeSource)



##########################################################################################################
# Event Calendars
##########################################################################################################
if doEvents is True:
    startTime = datetime.now()

    pages_list = [
    	 "https://www.trumba.com/calendars/sea_campus.xml?filterview=No+Ongoing+Events&filter2=_410447_417393_410450_410448_410449_410454_410453_410456_422910_&filter5=_409198_&filterfield2=30073&filterfield5=30051&events=500"
    	,"https://www.trumba.com/calendars/bot_campus.xml?filter1=_426473_426475_711388_1048732_426476_426477_426478_1092701_426480_426482_426483_426484_1054479_426486_&filterfield1=30442&events=500"
    	,"https://www.trumba.com/calendars/tac_campus.xml?filter1=_410447_417393_410450_410448_410449_410454_410453_410456_422910_&filterfield1=30073&events=500"
    	]
    
    scrapeSource = "UW Trumba Calendar"
    scrapeCategory = "Event"


    for target_url in pages_list:
        page = requests.get(target_url)
        soup = BeautifulSoup(page.content, 'lxml')
    
        entries = soup.find_all('entry')
        for entry in entries:

            #Gets content blob as separate component - soup it up
            contentContainer = BeautifulSoup(str(entry.find('content').get_text()), 'lxml')
        
            #pull out what OUGHT to be the date info for the event
            dateStr = contentContainer.br.previous_element
        
            splitted = dateStr.split('–')[0].strip()
            trailingSplit = splitted[-3:].strip()
            trailing = dateStr[-5:].strip()
        
            if trailingSplit == '.m.':
                dateStr = splitted
            else:
                dateStr = splitted + ' ' + trailing
        
            #see if we can coerce a date from the beginning inferred date string
            # if so - append a set of records to the df_* arrays
            # otherwise, do nothing.
            for d in datefinder.find_dates(dateStr):
                if d :
                    df_ArticleDate.append(d)
    
                    df_ArticleCategory.append(scrapeCategory)

                    df_ArticleURL.append(entry.link['href'])
                    df_ArticleTitle.append(entry.title.get_text().strip())
                    df_ArticleAuthor.append(entry.author.get_text().strip())
        
                    #remove the first navigble string and replace it with zero-length string
                    #functionally strips the date/time out of the description
                    contentContainer.br.previous_element.replace_with("")
                    df_ArticleContent.append(contentContainer.get_text().strip())

                    #future improvements - extract other structured data
                    df_ArticleSubTitle.append("")
                    df_ArticleFirstImage.append("")
                    df_ArticleTags.append("")
                    df_ArticleSideMatter.append("")
                
        time.sleep(wait_interval) #delay time requests are sent so we don't get kicked by server


    endTime = datetime.now()
    runTime = endTime - startTime
    print("----------------------------------------------------------------------------------------------------------")
    print( "{:2.2f} minutes to scrape "   .format( runTime.total_seconds()  / 60 ) + "from " + scrapeSource)


##########################################################################################################
# Save results in Excel file
##########################################################################################################
startTime = datetime.now()


data={'URL':df_ArticleURL,
      'Category':df_ArticleCategory,
      'Title':df_ArticleTitle,
      'SubTitle':df_ArticleSubTitle,
      'Author':df_ArticleAuthor,
      'Date':df_ArticleDate,
      'Tags':df_ArticleTags,
      'FirstImage':df_ArticleFirstImage,
      'SideMatter':df_ArticleSideMatter,
      'Content':df_ArticleContent,
      'ScrapeDate':datetime.now()}

#oldnews = pd.read_excel('corpus.xlsx')
news = pd.DataFrame(data=data)
cols = ['URL','Category','Title','SubTitle','Author','Date','Tags','FirstImage','SideMatter','Content','ScrapeDate']
news = news[cols]
#corpus_news = oldnews.append(news, sort=True)
news.drop_duplicates(subset='URL', keep='last', inplace=True)
news.reset_index(inplace=True)
news.drop(labels='index', axis=1, inplace=True)

filename = 'corpus.xlsx'
wks_name = 'Data'
writer = pd.ExcelWriter(filename)
news.to_excel(writer, wks_name, index=False)
writer.save()


endTime = datetime.now()
runTime = endTime - startTime
print("----------------------------------------------------------------------------------------------------------")
print( "{:2.2f} minutes to write "   .format( runTime.total_seconds()  / 60 ) + "{:,} records to disk ".format(len(df_ArticleURL)))

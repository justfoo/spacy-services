# coding: utf8
from __future__ import unicode_literals

import hug
from hug_middleware_cors import CORSMiddleware
import spacy
import pickle
import numpy as np
import pandas as pd

#Import pickled dataframe (faster than regenerating it)
#hardcoding this feels dirty.
#I need to make this understand the file's current location and look for the pickle there
corpus = pd.read_pickle("/var/www/spacy-services/uw/dfSimilarity.pkl")

MODELS = {
    "en_core_web_sm": spacy.load("en_core_web_sm"),
    "en_core_web_md": spacy.load("en_core_web_md"),
    "en_core_web_lg": spacy.load("en_core_web_lg"),
    "de_core_news_sm": spacy.load("de_core_news_sm"),
    "es_core_news_sm": spacy.load("es_core_news_sm"),
    "pt_core_news_sm": spacy.load("pt_core_news_sm"),
    "fr_core_news_sm": spacy.load("fr_core_news_sm"),
    "it_core_news_sm": spacy.load("it_core_news_sm"),
    "nl_core_news_sm": spacy.load("nl_core_news_sm"),
}


def get_model_desc(nlp, model_name):
    """Get human-readable model name, language name and version."""
    lang_cls = spacy.util.get_lang_class(nlp.lang)
    lang_name = lang_cls.__name__
    model_version = nlp.meta["version"]
    return "{} - {} (v{})".format(lang_name, model_name, model_version)


@hug.get("/models")
def models():
    return {name: get_model_desc(nlp, name) for name, nlp in MODELS.items()}


@hug.post("/dep")
def dep(
    text: str,
    model: str,
    collapse_punctuation: bool = False,
    collapse_phrases: bool = False,
):
    """Get dependencies for displaCy visualizer."""
    nlp = MODELS[model]
    doc = nlp(text)
    if collapse_phrases:
        for np in list(doc.noun_chunks):
            np.merge(tag=np.root.tag_, lemma=np.root.lemma_, ent_type=np.root.ent_type_)
    options = {"collapse_punct": collapse_punctuation}
    return spacy.displacy.parse_deps(doc, options)


@hug.post("/ent")
def ent(text: str, model: str):
    """Get entities for displaCy ENT visualizer."""
    nlp = MODELS[model]
    doc = nlp(text)
    return [
        {"start": ent.start_char, "end": ent.end_char, "label": ent.label_}
        for ent in doc.ents
    ]


@hug.post("/sim")
def ent(text: str, model: str):
    """Get similar content from saved documents."""
    nlp = MODELS[model]
    doc = nlp(text)

    ###Eventually make these parameters I can pass in
    numResults = 5
    matchThreshold = .6
    allowedTypes = pd.DataFrame([{'key':'E', 'fullName':'Event'},
                                 {'key':'G', 'fullName':'Gift Fund'},
                                 {'key':'K', 'fullName':'KUOW Content'},
                                 {'key':'M', 'fullName':'Magazine Article'},
                                 {'key':'N', 'fullName':'News Article'}
                                 ])

    ###Get vectors for the provided text for comparisons
    docVector = doc.vector
    docVectorNorm = doc.vector_norm

    #Calculate the similiarity w/ vector dot products and store it back in the dataframe
    simCalc = []
    for i in corpus.index:
        simCalc.append(np.dot(docVector, corpus['Vector'][i]) / (docVectorNorm * corpus['VectorNorm'][i]))
    corpus['Score'] = simCalc

    #Iterate through the types and pull the top X of each
    resultData = pd.DataFrame()
    for a_type in allowedTypes.fullName:
        data = corpus \
                .sort_values('Score', ascending=False) \
                .loc[ (corpus['Category'] == a_type) & (corpus['Score'] >= matchThreshold) ] \
                [['Category','Score','URL','Title']][0:numResults]
        #Append top X to the result data I'm passing back
        resultData=resultData.append(data,ignore_index=True)


    ###
    return [
        {"Category": getattr(row, "Category"), "Title": getattr(row, "Title"), "URL": getattr(row, "URL"), "Score": getattr(row, "Score")}
            for row in resultData.itertuples(index=True, name='Pandas')
    ]


if __name__ == "__main__":
    import waitress

    app = hug.API(__name__)
    app.http.add_middleware(CORSMiddleware(app))
    waitress.serve(__hug_wsgi__, listen="*:80 *:443")
